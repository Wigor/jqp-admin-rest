package com.jqp.admin.rbac.service;

import com.jqp.admin.rbac.data.TimerTask;

public interface TimerTaskApi {
    String execute(TimerTask task);
}
